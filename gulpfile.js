'use strict';

var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));


var tsSourceFiles = 'source/scripts/**/*.ts';

var scssSourceFiles = 'source/styles/**/*.scss';
var scssTargetFiles = 'source/dist/styles';

gulp.task('default', function(done_callback) {
    console.log('No default task existing');
    done_callback();
});

gulp.task('style:clean', function(done_callback) {
    let exec = require('child_process').exec,
    script = exec('rm -rf ./source/dist/styles');
    script.on('exit', function() {
        done_callback();
    });
});

gulp.task('script:clean', function(done_callback) {
    let exec = require('child_process').exec,
    script = exec('rm -rf ./source/dist/scripts');
    script.on('exit', function() {
        done_callback();
    });
});

gulp.task('style:build', function() {
    return gulp
        .src(scssSourceFiles)
        .pipe(sass({ style: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(scssTargetFiles))
});

gulp.task('style:watch', function() {
    console.log('Watching for changes in *.scss');
    gulp.watch(scssSourceFiles, gulp.series('style:build'));
});


gulp.task('script:build', function(done_callback) {
    let exec = require('child_process').exec,
    script = exec('./node_modules/webpack-cli/bin/cli.js --config ./webpack.config.js --mode production');
    script.stdout.on('data', function(data) {
        console.log(data.toString());
    });
    script.stderr.on('data', function(data) {
        console.error(data.toString());
    });
    script.on('exit', function(code) {
        console.log(`Webpack finished with StatusCode ${code}`);
        gulp.src('./source/scripts/lib/*.js').pipe(gulp.dest('./source/dist/scripts/lib/'));
        done_callback();
    });
});

gulp.task('script:watch', function() {
    gulp.watch(tsSourceFiles, gulp.series('script:build'))
});

gulp.task('build', gulp.parallel(['style:build', 'script:build']));

gulp.task('watch', gulp.parallel(['style:watch', 'script:watch']));
