import './prism';

document.addEventListener("DOMContentLoaded", function() {
    window.onscroll = function() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        document.getElementById('nav-post-progress-bar').style.width = (winScroll / height) * 100 + "%";
    };
});
