export interface EmojiwallConfiguration {
    canvasId: string | null;
    maxEmojis: number | null;
    animationDelay: number | null;
    enableEmojiCleanup: boolean | null;
}

export class Emojiwall {
    private enmojiCounter = 0;
    private emojiwallCanvas: HTMLElement; 
    private emojiwallConfiguration: EmojiwallConfiguration;

    private observerOptions: IntersectionObserverInit = {
        root: null,
        rootMargin: '0px',
        threshold: 0
    }

    private intersectionCallback = (entries: IntersectionObserverEntry[]) => {
        entries.forEach((entry: IntersectionObserverEntry) => {
            if (entry.isIntersecting === false) {
                this.removeEmojiElement(entry.target);
            }
        });
    }

    private viewPortIntersectionObserver = new IntersectionObserver(this.intersectionCallback, this.observerOptions);

    private applyConfigurationDefaults(forConfiguration: EmojiwallConfiguration=null): EmojiwallConfiguration {
        return {
            'canvasId': forConfiguration?.canvasId ?? 'particles',
            'maxEmojis': forConfiguration?.maxEmojis ?? 300,
            'animationDelay': forConfiguration?.animationDelay ?? 50,
            'enableEmojiCleanup': forConfiguration?.enableEmojiCleanup ?? true
        };
    }

    public initializeEmojiwall(withConfiguration: EmojiwallConfiguration=null): void {
        this.emojiwallConfiguration = this.applyConfigurationDefaults(withConfiguration);
        this.emojiwallCanvas = document.getElementById(this.emojiwallConfiguration.canvasId);
        
        if(this.emojiwallCanvas === null) {
            console.warn(`Cannot initialize emojiwall. a container with Id "${withConfiguration.canvasId}" was not found.`);
            return;
        }

        console.debug('Successfully initialized emojiwall with settings', this.emojiwallConfiguration);
    }

    private removeEmojiElement(elem: Element): void {
        elem.remove();
        this.enmojiCounter--;
    }

    private getRandomNumber(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    private setRandomLeftPosition(element: HTMLElement): void {
        element.style.left = this.getRandomNumber(0, 99) + '%';
    }

    private buildUnicodeEmojiContainer(emoji: string): HTMLDivElement {
        const emojiDiv = document.createElement('div');
        emojiDiv.classList.add('emojiwall-emoji');
        emojiDiv.innerHTML = emoji;
        return emojiDiv;
    }

    private buildImageEmojiContainer(emojiUrl: string): HTMLDivElement {
        const emojiImg = document.createElement('img');
        emojiImg.classList.add('emojiwall-emoji');
        emojiImg.src = emojiUrl;
        return emojiImg;
    }

    public addEmojis(emojis: string[]): void {
        const newEmojis: HTMLElement[] = [];
        emojis.forEach((element) => {
            if (this.enmojiCounter > this.emojiwallConfiguration.maxEmojis) {
                this.removeEmojiElement(this.emojiwallCanvas.firstElementChild);
            }

            const emojiContainer = element.startsWith('/')
                ? this.buildImageEmojiContainer(element)
                : this.buildUnicodeEmojiContainer(element);

            this.setRandomLeftPosition(emojiContainer);

            this.emojiwallCanvas.appendChild(emojiContainer);
            newEmojis.push(emojiContainer);
            this.enmojiCounter++;
        });

        setTimeout(() => {
            for (let i = 0; i < newEmojis.length; i++) {
                const left_position = this.getRandomNumber(0, 99);
                const duration = this.getRandomNumber(5, 20);
                const size = this.getRandomNumber(40, 120);
                const delay = this.getRandomNumber(20, 40);
                const easing_functions = ['ease', 'ease-in', 'ease-out', 'ease-in-out', 'linear'];
                const easing = easing_functions[this.getRandomNumber(0, easing_functions.length - 1)];

                newEmojis[i].style.height = `${size}px`;
                newEmojis[i].style.fontSize = `${size}px`;
                newEmojis[i].style.left = `${left_position}%`;
                newEmojis[i].style.bottom = '0px';
                newEmojis[i].style.top = '-250px';
                newEmojis[i].style.transition = `top ${duration}s ${easing} ${delay}ms, left ${duration}s ${easing} ${delay}ms`;
                
                if(this.emojiwallConfiguration.enableEmojiCleanup) 
                    this.viewPortIntersectionObserver.observe(newEmojis[i]);
            }
        }, this.emojiwallConfiguration.animationDelay);
    }  
}