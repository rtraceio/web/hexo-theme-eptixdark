export class Hello {
    private dataContent: string;

    constructor(){
        this.dataContent = 'Hello World'
    }

    public printIt(): void {
        console.log(this.dataContent);
    }

}