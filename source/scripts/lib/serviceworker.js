self.addEventListener('install', function(e) {
    console.log('PWA installed');
});

self.addEventListener('fetch', function(e) {
    // Network First, Cache from Network, On Network Failure read from Cache
    e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
});