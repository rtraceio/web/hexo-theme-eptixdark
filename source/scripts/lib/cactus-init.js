document.addEventListener('DOMContentLoaded', () => {
  console.info('This comment-section is powered by Cactus Comments. Your comments are actually messages on Matrix! Check out Cactus Comments: https://cactus.chat/');

  var commentSectionId = document
    .getElementById("comment-section-section-id").value
    .replaceAll('_', '-')
    .replaceAll('/', '')
    .replace('posts', '')
    .trim();

  initComments({
    node: document.getElementById("comment-section"),
    defaultHomeserverUrl: document.getElementById("comment-section-homeserver-url").value,
    serverName: document.getElementById("comment-section-server-name").value,
    siteName: document.getElementById("comment-section-site-name").value,
    commentSectionId: commentSectionId
  });
});