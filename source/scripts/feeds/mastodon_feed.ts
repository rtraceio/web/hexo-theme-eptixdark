interface MastodonAccount {
    id: string;
    username: string;
    acct: string;
    display_name: string;
    locked: boolean;
    bot: boolean;
    discoverable: boolean;
    group: boolean;
    created_at: string;
}

interface MastodonTag {
    name: string;
    url: string;
}

interface MastodonMention {
    id: string;
    username: string;
    url: string;
    acct: string;
}

interface MastodonEmoji {
    shortcode: string;
    url: string;
    static_url: string;
    visible_in_picker:boolean;
}


interface MastodonStatus {
    id: string;
    created_at: string;
    in_reply_to_id: string;
    in_reply_account_id: string;
    sensitive: boolean;
    spoiler_text: string;
    visibility: string;
    language: string;
    uri: string;
    url: string;
    replies_count: number;
    reblogs_count: number;
    favourites_count: number;
    favourited: boolean;
    reblogged: boolean;
    muted: boolean;
    bookmarked: boolean;
    pinned: boolean;
    content: string;
    account: MastodonAccount;
    media_attachments: any;
    tags: Array<MastodonTag>;
    emojis: Array<MastodonEmoji>;
    mentions: Array<MastodonMention>;
}

class MastodonUserFeed {
    public MastodonFeed() {

    }
}