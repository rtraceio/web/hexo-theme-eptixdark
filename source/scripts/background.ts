
import { tsParticles, MoveDirection } from "@tsparticles/engine";
import { loadFull } from "tsparticles";

import { Emojiwall } from "./emojiwall";




document.addEventListener('DOMContentLoaded', () => {
    const backgrounndAnimationInput = <HTMLInputElement>document.getElementById("theme-background-type");
    const backgroundAnimation = backgrounndAnimationInput?.value ?? 'default';

    if(backgroundAnimation === 'emojiwall') {
        const emotes = [
            '😀', '😁', '😂', '😃', '😄', '😅', '😆', '😆', '😇', '😈', '😉',
            '😊', '😋', '😌', '😍', '😎', '😗', '😘', '😙', '😚', '😛', '😜',
            '😝', '😬', '😴', '🙂', '🙃', '🛸', '🤓', '🤔', '🤖', '🤗', '🤞',
            '🤨', '🤩', '🤭', '🤯', '🐇', '🐈', '🐒', '🐝', '🐿️', '❤️', '❤️',
            '/img/emojicast/200iq.gif',
            '/img/emojicast/beebobble.gif',
            '/img/emojicast/bongocat.gif',
            '/img/emojicast/bongoroo.gif',
            '/img/emojicast/bonk.gif',
            '/img/emojicast/boomer.gif',
            '/img/emojicast/catjam.gif',
            '/img/emojicast/catkiss.gif',
            '/img/emojicast/confusedcat.png',
            '/img/emojicast/dogedance.gif',
            '/img/emojicast/duckdance.gif',
            '/img/emojicast/eyebrows.gif',
            '/img/emojicast/goatdance.gif',
            '/img/emojicast/hackerman.gif',
            '/img/emojicast/harold.png',
            '/img/emojicast/jammies.gif',
            '/img/emojicast/kumapls.gif',
            '/img/emojicast/myaacat.gif',
            '/img/emojicast/noted.gif',
            '/img/emojicast/pepebonk.gif',
            '/img/emojicast/pepeclap.gif',
            '/img/emojicast/pepecomfy.gif',
            '/img/emojicast/pepedj.gif',
            '/img/emojicast/pepegiggles.gif',
            '/img/emojicast/pepehonk.gif',
            '/img/emojicast/pepehyper.gif',
            '/img/emojicast/pepejam.gif',
            '/img/emojicast/pepelaugh.gif',
            '/img/emojicast/pepelaugh.gif',
            '/img/emojicast/pepenope.gif',
            '/img/emojicast/peperee.png',
            '/img/emojicast/pepeshoot.gif',
            '/img/emojicast/pepeshy.gif',
            '/img/emojicast/pogslide.gif',
            '/img/emojicast/schmusekadser.png',
            '/img/emojicast/stickmandance.gif',
            '/img/emojicast/trollface.png',
            '/img/emojicast/weewoo.gif',
            '/img/emojicast/youtried.gif'
        ];

        var emojiwall = new Emojiwall();
        emojiwall.initializeEmojiwall({
            'canvasId': 'particle-background',
            'maxEmojis': 100,
            'animationDelay': 1000,
            'enableEmojiCleanup': false
        });

        setInterval(() => {
            var randomEmoji = emotes[Math.floor(Math.random() * emotes.length)]
            emojiwall.addEmojis([randomEmoji]);
        }, 2000);
        return;
    }


    (async () => {
        await loadFull(tsParticles);
        await tsParticles
            .load({
                id: 'particle-background', 
                options: {
                background: {
                    color: "#101014",
                },
                particles: {
                    number: {
                        value: 50,
                    },
                    move: {
                        direction: MoveDirection.bottom,
                        enable: true,
                        random: false,
                        straight: false,
                    },
                    opacity: {
                        value: { min: 0.1, max: 0.5 },
                    },
                    size: {
                        value: { min: 1, max: 6 },
                    },
                    wobble: {
                        distance: 20,
                        enable: true,
                        speed: {
                            min: -3,
                            max: 3,
                        },
                    },
                },
            }})
            .then((c) => {
                console.debug('particlets initilaized');
                c.play();
            })
            .catch((e) => console.error(e));
    })();
});
