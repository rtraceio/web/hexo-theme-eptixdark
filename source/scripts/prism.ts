
import '../../node_modules/prismjs/dependencies';
import '../../node_modules/prismjs/prism';
import '../../node_modules/prismjs/components';

import '../../node_modules/prismjs/plugins/toolbar/prism-toolbar';
import '../../node_modules/prismjs/plugins/line-numbers/prism-line-numbers';
import '../../node_modules/prismjs/plugins/show-language/prism-show-language';
import '../../node_modules/prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard';
import '../../node_modules/prismjs/plugins/highlight-keywords/prism-highlight-keywords';

import '../../node_modules/prismjs/components/prism-markup-templating';
import '../../node_modules/prismjs/components/prism-c';
import '../../node_modules/prismjs/components/prism-go';
import '../../node_modules/prismjs/components/prism-sql';
import '../../node_modules/prismjs/components/prism-css';
import '../../node_modules/prismjs/components/prism-php';
import '../../node_modules/prismjs/components/prism-sass';
import '../../node_modules/prismjs/components/prism-json';
import '../../node_modules/prismjs/components/prism-bash';
import '../../node_modules/prismjs/components/prism-ruby';
import '../../node_modules/prismjs/components/prism-rust';
import '../../node_modules/prismjs/components/prism-java';
import '../../node_modules/prismjs/components/prism-latex';
import '../../node_modules/prismjs/components/prism-nginx';
import '../../node_modules/prismjs/components/prism-fsharp';
import '../../node_modules/prismjs/components/prism-csharp';
import '../../node_modules/prismjs/components/prism-docker';
import '../../node_modules/prismjs/components/prism-markup';
import '../../node_modules/prismjs/components/prism-python';
import '../../node_modules/prismjs/components/prism-xml-doc';
import '../../node_modules/prismjs/components/prism-protobuf';
import '../../node_modules/prismjs/components/prism-markdown';
import '../../node_modules/prismjs/components/prism-javascript';
import '../../node_modules/prismjs/components/prism-typescript';
import '../../node_modules/prismjs/components/prism-powershell';
