document.addEventListener("DOMContentLoaded", function() {
    const modal = document.getElementsByClassName("image-gallery-preview-modal")[0] as HTMLElement;
    const modalImg = document.getElementsByClassName("image-gallery-preview-modal-img")[0] as HTMLImageElement;
    const modalCaption = document.getElementsByClassName("image-gallery-preview-modal-caption")[0] as HTMLDivElement;

    const galleryElements = document
        .getElementsByClassName("css-article")[0]
        .getElementsByTagName("img");

    Array.from(galleryElements).forEach(element => {
        if(element.width >= 512) {
            element.onclick = () => {
                var imgSrc = element.src;
                var captionText = element.alt;
                modal.style.display = "block";
                modalImg.src = imgSrc;
                modalCaption.innerHTML = captionText;
            }
        }
    });

    var closeButton = modal.getElementsByClassName("image-gallery-preview-modal-close")[0] as HTMLButtonElement;
    const closeAction = () => modal.style.display = "none";
    closeButton.onclick = closeAction;

    document.addEventListener('keydown', (e) => {
        if ((e as KeyboardEvent).key === 'Escape') {
            closeAction();
        }
    }); 
});