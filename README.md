# Eptix-Dark

Eptix-Dark is a simplistic, minimalistic, responsive dark-theme for simple blogs. A Demo of this Theme can be found on [blog.rtrace.io](https://blog.rtrace.io).

## Screenshots

![Index](docs/index.png)
![Post](docs/post.png)
![Footer](docs/footer.png)

## Features

### Blog Features

- [x] Responsive (optimized for mobile view-ports)
- [x] OpenGraph for Articles (for improved SEO)
- [x] JSON-LD support for articles (for improved SEO)
- [x] Twitter Cards (for improved SEO)
- [x] Article page
- [x] Tagable Articles
- [x] Archives
- [x] Embed SoundCloud Tracks and playlists
- [x] Embed Mixcloud Tracks and playlists
- [x] Embed YouTube videos (through invidous)
- [x] Embde YouTube native videos
- [x] Embed asciinema recordings
- [x] Embed privacy-friendly Comment-Section (through Cactus Comments)
- [x] selectino between multiple backgrounds
- [x] Privacy friendly comment section
- [x] RSS Feed
- [x] ATOM Feed

### Markdown Features

- [x] rendering H1-H6
- [x] rendering paragraphs
- [x] rendering blockquotes
- [x] rendering markdown tables
- [x] rendering code-blocks with syntax highlighting
- [x] rendering ordered-lists and unordered-lists

## Libraries, Frameworks and Dependencies (and KUDOS)

- [PrismJS](https://prismjs.com/)
- [NormalizeCSS](https://necolas.github.io/normalize.css/)
- [ResetCSS (Eric Meyer)](https://meyerweb.com/eric/tools/css/reset/)
- [ParticleJS (Vincent Garreau)](https://github.com/VincentGarreau/particles.js/)
- [tsParticles (Matteo Bruni)](https://github.com/matteobruni/tsparticles)
- [Nunjucks Template Engine](https://mozilla.github.io/nunjucks/)
- [Hexo](https://hexo.io/)
- [Cactus-Comments](https://cactus.chat)
- [Montserrat Font (Julieta Ulanovsky)](https://github.com/JulietaUla/Montserrat/)
