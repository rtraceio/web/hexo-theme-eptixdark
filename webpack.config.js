const path = require('path');

module.exports = {
  entry: {
    'main': './source/scripts/index.ts',
    'page': './source/scripts/page.ts',
    'post': './source/scripts/post.ts',
    'prism': './source/scripts/prism.ts',
    'emojiwall': './source/scripts/emojiwall.ts',
    'background': './source/scripts/background.ts',
    'image_modal': './source/scripts/image_modal.ts'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'source', 'dist', 'scripts'),
  },
};