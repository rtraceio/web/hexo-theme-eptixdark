#!/bin/bash

image_dir='./source/img'
resolutions=('256x256' '128x128' '64x64' '32x32' '16x16')
fileextensions=('.png' '.jpg')

apk --update add imagemagick


for fileextension in "${fileextensions[@]}"; do
    for filepath in "$image_dir"/*"$fileextension"; do
        for resolution in "${resolutions[@]}"; do
            filename="$(basename "$filepath" "$fileextension")"
            if [[ "*" != "$filename" ]]; then
                magick convert -resize "$resolution" "$filepath" "$image_dir/$filename-$resolution$fileextension"
            fi
        done
    done
done
