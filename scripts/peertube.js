hexo.extend.tag.register('peertube', function(args, content) {
    var url = args[0];
    var sanitizedId = url
        .replace('https://', '')
        .replace(/[^a-zA-Z ]/g, '');

    return `
    <div class="markdown peertube-embed video-embed">
        <iframe
            id="peertube-${sanitizedId}"
            src="${url}"
            allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
            sandbox="allow-same-origin allow-scripts allow-popups"
            allowfullscreen>
        </iframe>
    </div>`;
});
