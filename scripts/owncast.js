hexo.extend.tag.register('owncast', function(args, content) {
    var id = args[0];

    return `
    <div class="markdown owncast-embed video-embed">
        <iframe
            id="owncast-${id}"
            src="${id}/embed/video"
            title="Owncast"
            height="350" width="550"
            referrerpolicy="origin"
            scrolling="no"
            allowfullscreen>
        </iframe>
    </div>`;
});