hexo.extend.tag.register('asciinema', function(args, content) {
    var id = args[0];
    return `<script id="asciicast-${id}" src="https://asciinema.org/a/${id}.js" async></script>`
});

