hexo.extend.tag.register('youtube', function(args, content) {
    return `<div class="youtube-embed video-embed">
            <iframe
                src="https://www.youtube-nocookie.com/embed/${args[0]}"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
                allowtransparency
            ></iframe>
        </div>`;
});





