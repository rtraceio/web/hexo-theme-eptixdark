hexo.extend.tag.register('invidious', function(args, content) {
    var id = args[0];
    var sanitizedId = id.replace(/[^a-zA-Z ]/g, '');
    
    var instances = [
        { 
            'active': true,
            'url': 'https://yt.rtrace.io', 
            'embedUrl': `https://yt.rtrace.io/embed/${id}`,
            'srcUrl': `https://yt.rtrace.io/watch?v=${id}`,
            'name': 'yt.rtrace.io', 
            'maintainer': 'ruffy', 
            'maintainerUrl': 'https://blog.rtrace.io/about/me'
        }
    ];

    var activeInstances = instances.filter(f => f['active'] === true);
    var instance = activeInstances[Math.floor(Math.random() * activeInstances.length)];;

    return `
    <div class="markdown video-embed">
        <iframe
            id="invidious-${sanitizedId}"
            src="${instance['embedUrl']}?autoplay=0" 
            title="Invidious"
            allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
            autoplay="false">
        </iframe>
    </div>`;
});

