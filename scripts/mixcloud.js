const axios = require('axios');

function mapToEmbed(embedId) {
    const encodedEmbedId = encodeURIComponent(embedId);
    return `
          <div class="mixcloud-embed embed">
              <iframe class="mixcloud-embed embed"
                      src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=${encodedEmbedId}">
              </iframe>
          </div>`;
}

function stripMixcloudHost(mixcloudUrl) {
    return mixcloudUrl
    .replace('http://www.mixcloud.com', '')
    .replace('https://www.mixcloud.com', '')
    .replace('www.mixcloud.com', '')
    .replace('mixcloud.com', '')
    .trim();
}

hexo.extend.tag.register('mixcloud', async (args, _) => {
    const src = stripMixcloudHost(args[1])

    if (args[0] == 'playlist') {
        const maxItems = Number(args[2] || 10);
        const apiUrl = `https://api.mixcloud.com${src}cloudcasts`;

        return axios
            .get(apiUrl)
            .then(response => {
                return response.data.data
                    .map(entry => entry.url)
                    .map(url => stripMixcloudHost(url))
                    .map(embedId => mapToEmbed(embedId))
                    .slice(0, maxItems)
                    .join('');
            });
    }

    if (args[0] == 'track') {
        return mapToEmbed(src);
    }


}, { async: true });
